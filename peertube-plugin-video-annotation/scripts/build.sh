#!/bib/sh

rm -rf ./dist ./assets/videojs-overlay.css

node ./scripts/esbuild.js

cp ./node_modules/videojs-overlay/dist/videojs-overlay.css ./assets/videojs-overlay.css
